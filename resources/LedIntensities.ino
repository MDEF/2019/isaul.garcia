int data;
const int threshold = 400;

int BUTTONS = A0;
int LEDX = 11;
int LEDY = 10;
int LEDZ = 9;

void setup() {
  pinMode(LEDX, OUTPUT);
  pinMode(LEDY, OUTPUT);
  pinMode(LEDZ, OUTPUT);
  pinMode(BUTTONS, INPUT);
  Serial.begin(9600);

}

void loop() {
 
  data = analogRead(BUTTONS);
  Serial.print("BUTTONPRESS = ");
  Serial.println(data);
  
   if (data > 1000) {
    analogWrite(LEDX, 20);
    analogWrite(LEDY, 20);
    analogWrite(LEDZ, 20);
  }  
    else if (data >= (threshold)) {
    digitalWrite(LEDX, HIGH);
    digitalWrite(LEDY, LOW);
    digitalWrite(LEDZ, HIGH);
  }
    else if (data <= (threshold)) 
  {
    digitalWrite(LEDX, LOW);
    digitalWrite(LEDY, HIGH);
    digitalWrite(LEDZ, LOW);
  }
}
